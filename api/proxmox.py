import proxmoxer.core

from utils.config import get_config
from utils.vm import get_state_from_status, State
from proxmoxer import ProxmoxAPI, ProxmoxResource
from functools import reduce
import time

node_name = "ns397911"
template_vmid = 103


def get_node_resource() -> ProxmoxResource:
    config = get_config()
    proxmox = ProxmoxAPI(config["HOST"],
                         user=config["USER"] + "@pam",
                         token_name=config["TOKEN_NAME"],
                         token_value=config["TOKEN_SECRET"],
                         verify_ssl=False)
    node_resource = proxmox.nodes(node_name).qemu

    return node_resource


def create_and_start(name: str):

    node_resource = get_node_resource()

    new_id = max([int(x['vmid']) for x in node_resource.get()]) + 1

    template_clone_params = {
        'vmid': template_vmid,
        'newid': new_id,
        'node': node_name,
        'name': name
    }

    node_resource(template_vmid).clone.post(**template_clone_params)
    node_resource(new_id).status.start.post()

def get_vm_state(vmid: int):
    node_resource = get_node_resource()

    status = node_resource(vmid).status.current.get()

    return get_state_from_status(status)


vms = []


def get_vms(cache=False):
    global vms
    if cache:
        return vms

    vms = []

    node_resource = get_node_resource()

    for vm in node_resource.get():
        if vm['template'] == 1:
            continue

        vmid = int(vm['vmid'])

        if vmid in [100, 101]:
            continue

        state = get_vm_state(vmid)

        if state in [State.PAUSED, State.STOPPED]:
            vms.append({
                "name": vm['name'], "ip": "-", "domain": "", "id": vmid, 'state': str(state)
            })
            continue

        try:
            ip = get_vm_ip(vmid)
        except proxmoxer.core.ResourceException:
            continue
        vms.append({
            "name": vm['name'], "ip": ip, "domain": "", "id": vmid, 'state': str(state)
        })

    return vms


def get_vm_ip(vmid: int):
    node_resource = get_node_resource()

    data = node_resource(vmid).agent("network-get-interfaces").get()

    def reducer(ips: list, interface):
        ipv4 = list(
            filter(lambda protocol_version: protocol_version['ip-address-type'] == 'ipv4', interface['ip-addresses']))
        ips.append(ipv4[0]['ip-address'])
        return ips

    interfaces_not_lo = list(filter(lambda interface: interface['name'] != 'lo', data['result']))

    return reduce(reducer, interfaces_not_lo, [])[0]


def guard_permanent_vm(func):
    def inner(vmid):
        if vmid == 100:
            return
        return func(vmid)

    return inner


@guard_permanent_vm
def stop_and_destroy_vm(vmid: int):
    node_resource = get_node_resource()

    destroy_template = {
        'purge': 1
    }

    node_resource(vmid).status.stop.post()
    time.sleep(5)
    node_resource(vmid).delete(**destroy_template)


@guard_permanent_vm
def stop_vm(vmid: int):

    node_resource = get_node_resource()
    node_resource(vmid).status.shutdown.post()

def start_vm(vmid: int):

    node_resource = get_node_resource()
    node_resource(vmid).status.start.post()

def run():
    node_resource = get_node_resource()
    # create_and_start(node_resource, "RhoKapa")
    # create_and_start(node_resource, "test2")
    for i in range(5, 10):
        print(i)
        create_and_start("test" + str(i))
        # stop_and_destroy_vm(i)

    # get_network_config(node_resource, 105)
    # return get_network_config(node_resource, 105)[0]


if __name__ == "__main__":
    run()
