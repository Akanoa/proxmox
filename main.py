import asyncio
from typing import Optional

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from fastapi.staticfiles import StaticFiles

from starlette.applications import Starlette
from sse_starlette.sse import EventSourceResponse

from api.proxmox import get_vms, stop_vm, start_vm


async def numbers(minimum, maximum):
    for i in range(minimum, maximum + 1):
        await asyncio.sleep(0.9)
        yield dict(data=i, event='update')


async def vm_info():
    i = 0
    while True:
        i += 1
        await asyncio.sleep(5)
        vms = get_vms()
        yield dict(data=vms, event="update", id=i)


origins = [
    "http://localhost",
    "http://localhost:3000",
]

app = FastAPI()
# app.mount("/", StaticFiles(directory="static/dist", html=True), name="static")

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class VmStartStopRequest(BaseModel):
    vmid: int


@app.get("/api/vms")
def api_get_vms(cache: bool):
    return get_vms(cache)


@app.post('/api/vm/stop')
def api_stop_vm(vm_stop_request: VmStartStopRequest):
    return stop_vm(vm_stop_request.vmid)


@app.post('/api/vm/start')
def api_start_vm(vm_start_request: VmStartStopRequest):
    return start_vm(vm_start_request.vmid)


@app.get('/api/sse')
async def sse():
    generator = vm_info()
    return EventSourceResponse(generator)
