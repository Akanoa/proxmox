import time
import random

def generator_return():
    tab = []
    for i in range(3):
        time.sleep(3)
        tab.append(i)
    return tab

def generator_yield():

    while True:
        time.sleep(3)
        yield random.randint(0, 1000)

def run():

    for i in generator_yield():
        print(i)

    pass

if __name__ == "__main__":
    run()