const root = 'http://127.0.0.1:8000/api'

export const stopVm = async (id) => {
    await fetch(root+'/vm/stop', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({vmid: id})
        }
    )
}

export const startVm = async (id) => {
    await fetch(root+'/vm/start', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({vmid: id})
        }
    )
}

export const listenSSE = (vms) => {
    let source = new EventSource(root+"/sse")
    source.addEventListener("update", e => {
        //console.log(typeof e.data)
        const config = JSON.parse(e.data.replaceAll('\'', '"'))
        vms.value = config.sort((a, b) => a.id - b.id)
    }, false)
}

export const getVmsInfo = async (cache=false) => {
  return await fetch(`${root}/vms?cache=${cache}`)
      .then(response => response.json())
      .then(data => data.sort((a, b) => a.id - b.id))
}