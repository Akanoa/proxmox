import { createApp } from 'vue'
import {createRouter, createWebHistory} from "vue-router";
import ListVMs from "./components/ListVMs.vue";
import App from "./App.vue";
import CreateVM from "./components/CreateVM.vue";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {path: '/', component: ListVMs},
        {path: '/create', component: CreateVM},

    ]
})

const app = createApp(App)
app.use(router)

app.mount('#app')
