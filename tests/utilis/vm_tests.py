import unittest
from utils.vm import get_state_from_status, State


class VmState(unittest.TestCase):
    def test_paused(self):
        status = {'qmpstatus': 'paused', 'status': 'running'}
        self.assertEqual(get_state_from_status(status), State.PAUSED)

    def test_started(self):
        status = {'qmpstatus': 'running', 'status': 'running'}
        self.assertEqual(get_state_from_status(status), State.STARTED)

    def test_stopped(self):
        status = {'qmpstatus': 'stopped', 'status': 'stopped'}
        self.assertEqual(get_state_from_status(status), State.STOPPED)


if __name__ == '__main__':
    unittest.main()
