import os
from dotenv import dotenv_values

def get_config():
    path = os.path.dirname(os.path.abspath(__file__))
    env_file = os.path.join(path, '../.env')
    config = dotenv_values(env_file)
    return config
if __name__ == "__main__":
    get_config()