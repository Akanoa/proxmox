from enum import Enum, auto

class State(Enum):
    STARTED = auto()
    PAUSED = auto()
    STOPPED = auto()
    STARTING = auto()
    STOPPING = auto()
    DESTROYING = auto()
    UNKNOWN = auto()
    def __str__(self):
        match self:
            case State.PAUSED:
                return "paused"
            case State.STOPPED:
                return "stopped"
            case State.STARTED:
                return "started"


# https://github.com/qemu/qemu/blob/master/qapi/run-state.json
# { 'enum': 'RunState',
#   'data': [ 'debug', 'inmigrate', 'internal-error', 'io-error', 'paused',
#             'postmigrate', 'prelaunch', 'finish-migrate', 'restore-vm',
#             'running', 'save-vm', 'shutdown', 'suspended', 'watchdog',
#             'guest-panicked', 'colo' ] }

def get_state_from_status(status: dict):

    match (status['status'], status['qmpstatus']):
        case ("running", "paused"):
            return State.PAUSED
        case("running", "running"):
            return State.STARTED
        case("stopped", "stopped"):
            return State.STOPPED
        case (_, _):
            return State.UNKNOWN

